import { env } from 'process';

import {
  ClassSerializerInterceptor,
  Logger,
  ValidationPipe,
  VersioningType,
} from '@nestjs/common';
import { NestFactory, Reflector } from '@nestjs/core';

import { AppModule } from './app.module';

async function bootstrap(): Promise<void> {
  const app = await NestFactory.create(AppModule);
  const globalPrefix = 'api';
  const defaultVersion = env.DEFAULT_VERSION || '1';
  const port = env.PORT || 3000;
  const corsEnable = env.CORS_ENABLE === '1';

  // Validation
  app.useGlobalPipes(new ValidationPipe());
  app.useGlobalInterceptors(new ClassSerializerInterceptor(app.get(Reflector)));

  // cors
  if (corsEnable) {
    app.enableCors();
  }

  // Version
  app.enableVersioning({
    type: VersioningType.URI,
    defaultVersion: defaultVersion,
  });

  app.setGlobalPrefix(globalPrefix);
  await app.listen(port).then(() => {
    Logger.log(
      `🚀 Application is running on: http://localhost:${port}/${globalPrefix}/${defaultVersion}`
    );
  });
}

bootstrap().catch((error) => {
  new Logger().error(error.message, error);
  process.exit(1);
});
